//
//  AddCourtViewController.h
//  FBGHProject
//
//  Created by Anatoliy on 4/18/16.
//  Copyright © 2016 mobex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <Firebase/Firebase.h>

@interface AddCourtViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate, UIPickerViewAccessibilityDelegate, UITextFieldDelegate>

@property (nonatomic) CLLocationCoordinate2D coordinate;

- (void)showInView:(UIView *)aView animated:(BOOL)animated;

@end
