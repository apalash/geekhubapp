//
//  AddCourtViewController.m
//  FBGHProject
//
//  Created by Anatoliy on 4/18/16.
//  Copyright © 2016 mobex. All rights reserved.
//

#import "AddCourtViewController.h"

@interface AddCourtViewController ()
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIPickerView *myPickerView;
@property (weak, nonatomic) IBOutlet UITextField *courtNameTextField;
@property (weak, nonatomic) IBOutlet UIView *addCourtView;
@property (strong, nonatomic) NSArray *courtCoverTypes;

@end

@implementation AddCourtViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.6];
    self.addCourtView.layer.cornerRadius = 5;
    self.addCourtView.layer.shadowOpacity = 0.8;
    self.addCourtView.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    self.courtNameTextField.delegate = self;
    self.myPickerView.delegate = self;
    self.myPickerView.dataSource = self;
    self.courtCoverTypes = @[@"rubber", @"asphalt", @"wooden", @"concrete"];
    self.saveButton.enabled = NO;
    //[self.courtNameTextField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)textFieldClicked:(id)sender {
    [self.courtNameTextField becomeFirstResponder];
}

- (void)showInView:(UIView *)aView animated:(BOOL)animated {
    [aView addSubview:self.view];
    if (animated) {
        [self showAnimate];
    }
}

- (void)showAnimate {
    self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.view.alpha = 0;
    [UIView animateWithDuration:.25 animations:^{
        self.view.alpha = 1;
        self.view.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

- (void)removeAnimate {
    [UIView animateWithDuration:.25 animations:^{
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self.view removeFromSuperview];
        }
    }];
}

- (IBAction)textFieldEdited:(id)sender {
    self.saveButton.enabled = YES;
}

- (IBAction)cancelButtonClicked:(id)sender {
    [self removeAnimate];
}

- (IBAction)saveButtonClicked:(id)sender {
    Firebase *courtAddRef = [[Firebase alloc] initWithUrl:@"https://fbghproject.firebaseio.com/Courts"];
    NSDictionary *newCourt = @{
                               @"name" : self.courtNameTextField.text,
                               @"cover" : [self.courtCoverTypes
                                           objectAtIndex:[self.myPickerView  selectedRowInComponent:0]],
                               @"latitude" : [NSNumber numberWithFloat:self.coordinate.latitude],
                               @"longitude" : [NSNumber numberWithFloat:self.coordinate.longitude],
                               @"hasEvents" : @"free",
                               @"photo" : @""
                               };
    [[courtAddRef childByAutoId] setValue:newCourt];
    [self removeAnimate];
}

#pragma mark - PickerView DataSource 

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.courtCoverTypes count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [self.courtCoverTypes objectAtIndex:row];
}

#pragma mark - TextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
