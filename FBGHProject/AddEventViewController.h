//
//  ViewController.h
//  FBGHProject
//
//  Created by Anatoliy on 4/2/16.
//  Copyright © 2016 mobex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Firebase/Firebase.h>
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"

@interface AddEventViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *addEventView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) NSString *courtKey;
@property (nonatomic, strong) NSString *profileKey;

- (void)showInView:(UIView *)aView animated:(BOOL)animated;

@end

