//
//  ViewController.m
//  FBGHProject
//
//  Created by Anatoliy on 4/2/16.
//  Copyright © 2016 mobex. All rights reserved.
//

#import "AddEventViewController.h"

@interface AddEventViewController ()

@property (weak, nonatomic) IBOutlet UISwitch *mySwitch;
@property (weak, nonatomic) IBOutlet UITextField *championshipTextField;
@property (weak, nonatomic) IBOutlet UITextField *leagueTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *saveButtonTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cancelButtonTopConstraint;
@property (weak, nonatomic) IBOutlet UIDatePicker *secondDatePicker;

@end

@implementation AddEventViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.6];
    self.addEventView.layer.cornerRadius = 5;
    self.addEventView.layer.shadowOpacity = 0.8;
    self.addEventView.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    
    self.datePicker.minimumDate = [NSDate date];
    self.secondDatePicker.minimumDate = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [NSDateComponents new];
    dateComponents.month = 1;
    self.datePicker.maximumDate = [calendar dateByAddingComponents:dateComponents
                                                            toDate:[NSDate date]
                                                           options:0];
    calendar = [NSCalendar currentCalendar];
    dateComponents.hour = 12;
    self.secondDatePicker.maximumDate = [calendar dateByAddingComponents:dateComponents
                                                                  toDate:[NSDate date]
                                                                 options:0];
    self.championshipTextField.delegate = self;
    self.leagueTextField.delegate = self;
    self.championshipTextField.hidden = YES;
    self.leagueTextField.hidden = YES;
    self.saveButtonTopConstraint.constant = 10.0f;
    self.cancelButtonTopConstraint.constant = 10.0f;
    self.viewHeightConstraint.constant = 345.0f;
    self.saveButton.enabled = NO;
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showInView:(UIView *)aView animated:(BOOL)animated {
    [aView addSubview:self.view];
    if (animated) {
        [self showAnimate];
    }
}

- (void)showAnimate {
    self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.view.alpha = 0;
    [UIView animateWithDuration:.25 animations:^{
        self.view.alpha = 1;
        self.view.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

- (void)removeAnimate {
    [UIView animateWithDuration:.25 animations:^{
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self.view removeFromSuperview];
        }
    }];
}

- (IBAction)switchValueChanged:(id)sender {
    if (self.mySwitch.on) {
        self.viewHeightConstraint.constant = 415.0f;
        self.saveButtonTopConstraint.constant = 85.0f;
        self.cancelButtonTopConstraint.constant = 85.0f;
        [UIView animateWithDuration:1
                         animations:^{
                             [self.view layoutIfNeeded];
                         }
                         completion:^(BOOL finished) {
                             self.championshipTextField.hidden = NO;
                             self.leagueTextField.hidden = NO;
                         }];
    } else {
        self.championshipTextField.hidden = YES;
        self.leagueTextField.hidden = YES;
        self.viewHeightConstraint.constant = 345.0f;
        self.saveButtonTopConstraint.constant = 10.0f;
        self.cancelButtonTopConstraint.constant = 10.0f;
        [UIView animateWithDuration:1
                         animations:^{
                             [self.view layoutIfNeeded];
                         }];
    }
}

- (IBAction)secondDatePickerValueChanged:(id)sender {
    if ([self.secondDatePicker.date isEqualToDate:self.secondDatePicker.minimumDate]) {
        self.saveButton.enabled = NO;
    } else {
        self.saveButton.enabled = YES;
    }
}

- (IBAction)cancelButtonTouched:(id)sender {
    [self removeAnimate];
}

- (IBAction)saveButtonTouched:(id)sender {
    Firebase *eventRef = [[Firebase alloc] initWithUrl:@"https://fbghproject.firebaseio.com/Courts"];
    [[[eventRef childByAppendingPath:self.courtKey] childByAppendingPath:@"hasEvents"] setValue:@"yes"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yy HH:mm"];
    NSString *startDate = [dateFormatter stringFromDate:self.datePicker.date];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *finishDate = [dateFormatter stringFromDate:self.secondDatePicker.date];
    NSDictionary *participants = @{
                                   self.profileKey : self.profileKey
                                   };
    NSString *path;
    if ((![self.championshipTextField.text isEqual:@""]) ||
        (![self.leagueTextField.text isEqual:@""])){
         path = [[self.championshipTextField.text stringByAppendingString:@" "] stringByAppendingString:self.leagueTextField.text];
    } else {
        path = [[startDate stringByAppendingString:@"-"] stringByAppendingString:finishDate];
    }
    @try {
        NSDictionary *eventInfo = @{
                                    @"startDate" : startDate,
                                    @"endDate" : finishDate,
                                    @"championship" : self.championshipTextField.text,
                                    @"league" : self.leagueTextField.text,
                                    @"Participants" : participants
                                    };
        [[[[eventRef childByAppendingPath:self.courtKey] childByAppendingPath:@"Events"] childByAppendingPath:path] setValue:eventInfo];
        NSDate *start = [dateFormatter dateFromString:startDate];
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = [start dateByAddingTimeInterval:-1 * 60 * 60];
        localNotification.alertBody = [path stringByAppendingString:@" starts in an hour."];
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:localNotification];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:path];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        [self removeAnimate];
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception.description);
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Check all fields aren't empty!" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    @finally {
    }
}

#pragma mark - TextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
