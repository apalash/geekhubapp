//
//  Court.h
//  FBGHProject
//
//  Created by Anatoliy on 4/10/16.
//  Copyright © 2016 mobex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Firebase/Firebase.h"
#import <GoogleMaps/GoogleMaps.h>

@interface Court : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *cover;
//@property (nonatomic, strong) NSNumber *countOfCourts;
@property (nonatomic, strong) NSString *photo;
@property (nonatomic, strong) NSString *hasEvent;
@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic, strong) Firebase *ref;

@end
