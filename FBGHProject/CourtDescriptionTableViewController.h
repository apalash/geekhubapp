//
//  CourtDescriptionTableViewController.h
//  FBGHProject
//
//  Created by Anatoliy on 4/10/16.
//  Copyright © 2016 mobex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Court.h"
#import "AddEventViewController.h"
#import "PopCourtEventsTableViewController.h"

@interface CourtDescriptionTableViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate, UIPopoverPresentationControllerDelegate, UIPopoverControllerDelegate, UIPopoverBackgroundViewMethods>

@property (nonatomic, strong) NSString *courtKey;
@property (nonatomic, strong) NSString *profileKey;

@end
