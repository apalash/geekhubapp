//
//  CourtDescriptionTableViewController.m
//  FBGHProject
//
//  Created by Anatoliy on 4/10/16.
//  Copyright © 2016 mobex. All rights reserved.
//

#import "CourtDescriptionTableViewController.h"

@interface CourtDescriptionTableViewController ()

@property (nonatomic, strong) Firebase *courtsRef;
@property (weak, nonatomic) IBOutlet UIImageView *courtPhoto;
@property (weak, nonatomic) IBOutlet UILabel *courtNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *courtCoverLabel;

@end

@implementation CourtDescriptionTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Image-4.png"]];
    [tempImageView setFrame:self.tableView.frame];
    self.tableView.backgroundView = tempImageView;
    
    self.courtsRef = [[Firebase alloc] initWithUrl:@"https://fbghproject.firebaseio.com/Courts"];
    [[self.courtsRef childByAppendingPath:self.courtKey] observeEventType:FEventTypeValue  withBlock:^(FDataSnapshot *snapshot) {
        self.courtNameLabel.text = snapshot.value[@"name"];
        self.courtCoverLabel.text = snapshot.value[@"cover"];
        if ((![snapshot.value[@"photo"] isEqualToString:@""]) || (snapshot.value[@"photo"] != nil)) {
            self.courtPhoto = snapshot.value[@"photo"];
        }
    } withCancelBlock:^(NSError *error) {
        NSLog(@"%@", error.description);
    }];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)retrieveImage {
   // NSData *decodedData = [NSData alloc] initWithBase64EncodedString:<#(nonnull NSString *)#> options:<#(NSDataBase64DecodingOptions)#>
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)addCourtEventClicked:(id)sender {
    UIStoryboard *myStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddEventViewController *addEventView = [myStoryboard instantiateViewControllerWithIdentifier:@"AddEventViewController"];
    addEventView.courtKey = self.courtKey;
    addEventView.profileKey = self.profileKey;
    [self addChildViewController:addEventView];
    [addEventView showInView:self.view
                    animated:YES];
}

- (IBAction)eventsButtonClicked:(id)sender {
    // grab the view controller we want to show
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PopCourtEventsTableViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"PopCourtEvents"];
    controller.courtKey = self.courtKey;
    controller.userKey = self.profileKey;
    [self presentViewController:controller animated:YES completion:nil];
//    
//    // present the controller
//    // on iPad, this will be a Popover
//    // on iPhone, this will be an action sheet
//    controller.modalPresentationStyle = UIModalPresentationPopover;
//    [self presentViewController:controller animated:YES completion:nil];
//    
//    // configure the Popover presentation controller
//    UIPopoverPresentationController *popController = [controller popoverPresentationController];
//    popController.permittedArrowDirections = UIPopoverArrowDirectionUp;
//    popController.delegate = self;
//    
//    // in case we don't have a bar button as reference
//    popController.sourceView = self.view;
//    popController.sourceRect = CGRectMake(30, 50, 10, 10);
}

#pragma mark Load data from Firebase

- (void)loadFirebaseData {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = true;
    
    [self.courtsRef observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        NSMutableArray *court = [NSMutableArray new];
        
        for (FDataSnapshot *list in snapshot.children) {
            Court *courtList = [Court alloc];
            courtList = (Court *)list;
            [court addObject:courtList];
        }
        
       // self.courts = court;
        [self.tableView reloadData];
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = false;
    }];
}

@end
