//
//  CustomInfoWindow.h
//  FBGHProject
//
//  Created by Anatoliy on 4/14/16.
//  Copyright © 2016 mobex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomInfoWindow : UIView

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIImageView *photo;

@end
