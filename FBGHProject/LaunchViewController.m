//
//  LaunchViewController.m
//  FBGHProject
//
//  Created by Anatoliy on 4/3/16.
//  Copyright © 2016 mobex. All rights reserved.
//

#import "LaunchViewController.h"

static NSString * const kFirebaseURL = @"https://fbghproject.firebaseio.com";
static NSString * const kGoogleAPIKey = @"AIzaSyB7w9pGeyFbvxb0m2rg7Ant9Q4LwSaJJoU";
static NSString * const loginToMap = @"LoginToMap";
//static NSString * const loginToMapViaSplit = @"ShowSplitController";

@interface LaunchViewController ()
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (nonatomic, strong) Firebase *usersRef;
@property (weak, nonatomic) IBOutlet UIButton *signUpButton;
@property (weak, nonatomic) IBOutlet UIButton *logInButton;
@end

@implementation LaunchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.emailTextField.delegate = self;
    self.passwordTextField.delegate = self;    
    
    self.ref = [[Firebase alloc] initWithUrl:kFirebaseURL];
    self.usersRef = [[Firebase alloc] initWithUrl:@"https://fbghproject.firebaseio.com/Users"];
    // TODO(developer) Configure the sign-in button look/feel
    [GIDSignIn sharedInstance].delegate = self;
    
    [GIDSignIn sharedInstance].uiDelegate = self;
    
    // Uncomment to automatically sign in the user.
  // [[GIDSignIn sharedInstance] signInSilently];
    
    // [START_EXCLUDE silent]
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveToggleAuthUINotification:)
                                                 name:@"ToggleAuthUINotification"
                                               object:nil];
    
    
    [self toggleAuthUI];
    //[self statusText].text = @"Google Sign in\niOS Demo";
   // MapViewController *mapVC = [[MapViewController alloc] init];
   // [self.navigationController pushViewController:mapVC animated:YES ];
    // [END_EXCLUDE]
}

- (void)viewDidAppear:(BOOL)animated {
    [self.ref observeAuthEventWithBlock:^(FAuthData *authData) {
        if (authData != nil) {
            NSLog(@"%@", authData);
            //[self performSegueWithIdentifier:loginToMap sender:nil];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)logInTouched:(id)sender {
    [self.ref authUser:self.emailTextField.text password:self.passwordTextField.text withCompletionBlock:^(NSError *error, FAuthData *authData) {
        if (error == nil) {
            self.profileKey = [self.emailTextField.text stringByReplacingOccurrencesOfString:@"." withString:@""];
            [self initUserDefaults];
            [self performSegueWithIdentifier:loginToMap sender:nil];
        } else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:error.description preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }];
}

- (IBAction)signUpTouched:(id)sender {
    [self.ref createUser:self.emailTextField.text password:self.passwordTextField.text withCompletionBlock:^(NSError *error) {
        if (error == nil) {
            NSDictionary *newUserEmpty = @{
                                           @"name" : self.emailTextField.text,
                                           @"photo" : @"",
                                           @"position" : @"",
                                           @"contacts" : self.emailTextField.text
                                           };
            [[[self.ref childByAppendingPath:@"Users"] childByAppendingPath:[self.emailTextField.text stringByReplacingOccurrencesOfString:@"." withString:@""]] setValue:newUserEmpty];
            self.profileKey = [self.emailTextField.text stringByReplacingOccurrencesOfString:@"." withString:@""];
            [self performSegueWithIdentifier:loginToMap sender:nil];
            } else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:error.description preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        
    }];
}

- (void)initUserDefaults {
    Firebase *firebaseRef = [[Firebase alloc] initWithUrl:@"https://fbghproject.firebaseio.com/Courts"];
    [firebaseRef observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        for (FDataSnapshot *child in snapshot.children) {
            if ([child.value[@"hasEvents"] isEqualToString:@"yes"]) {
                Firebase *eventRef = [[firebaseRef childByAppendingPath:child.key] childByAppendingPath:@"Events"];
                [eventRef observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
                    for (FDataSnapshot *child in snapshot.children) {
                        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                        formatter.dateFormat = @"dd-MM-yy";
                        NSString *currentDate = [formatter stringFromDate:[NSDate date]];
                        NSArray *dateComponents = [child.value[@"startDate"] componentsSeparatedByString:@" "];
                        NSTimeInterval interval = [[formatter dateFromString:[dateComponents objectAtIndex:0]] timeIntervalSinceDate:[formatter dateFromString:currentDate]];
                        if (interval >= 0) {
                            [[[eventRef childByAppendingPath:child.key] childByAppendingPath:@"Participants"] observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
                                for (FDataSnapshot *grandChild in snapshot.children) {
                                    if ([grandChild.key isEqualToString:self.profileKey]) {
                                        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                        formatter.dateFormat = @"dd-MM-yy HH:mm";
                                        NSDate *startDate = [formatter dateFromString:snapshot.value[@"startDate"]];
                                        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
                                        localNotification.fireDate = [startDate dateByAddingTimeInterval:-1 * 60 * 60];
                                        localNotification.alertBody = [child.key stringByAppendingString:@" starts in an hour."];
                                        localNotification.timeZone = [NSTimeZone defaultTimeZone];
                                        localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                                        localNotification.soundName = UILocalNotificationDefaultSoundName;
                                        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:localNotification];
                                        [[NSUserDefaults standardUserDefaults] setObject:data forKey:child.key];
                                        [[NSUserDefaults standardUserDefaults] synchronize];
                                        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                                    }
                                }
                            }];
                        }
                    }
                }];
            }
        }
    }];
}

#pragma mark - TextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:loginToMap]){
        UINavigationController *navController = (UINavigationController *)segue.destinationViewController;
        MapViewController *controller = (MapViewController *)navController.topViewController;
        controller.profileKey = self.profileKey;
    }
}

#pragma mark Google auth

- (IBAction)didTapSignOut:(id)sender {
    [[GIDSignIn sharedInstance] signOut];
    [self toggleAuthUI];
}

- (IBAction)didTapDisconnect:(id)sender {
    [[GIDSignIn sharedInstance] disconnect];
    self.signInButtonGoogle.hidden = NO;
    self.signOutButton.hidden = YES;
    self.disconnectButton.hidden = YES;
}

- (void)toggleAuthUI {
    if ([GIDSignIn sharedInstance].currentUser.authentication == nil) {
        // Not signed in
        self.signInButtonGoogle.hidden = NO;
        self.signOutButton.hidden = YES;
        self.disconnectButton.hidden = YES;
        self.emailTextField.enabled = YES;
        self.passwordTextField.enabled = YES;
        self.logInButton.enabled = YES;
        self.signUpButton.enabled = YES;
    } else {
        // Signed in
        self.signInButtonGoogle.hidden = YES;
        self.signOutButton.hidden = NO;
        self.disconnectButton.hidden = NO;
        self.emailTextField.enabled = NO;
        self.passwordTextField.enabled = NO;
        self.logInButton.enabled = NO;
        self.signUpButton.enabled = NO;
        self.profileKey = [[GIDSignIn sharedInstance].currentUser.profile.email stringByReplacingOccurrencesOfString:@"." withString:@""];
        [self performSegueWithIdentifier:loginToMap sender:nil];
    }
}


- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    // Perform any operations on signed in user here.
    @try {
        NSString *userId = user.userID;                  // For client-side use only!
        NSString *idToken = user.authentication.idToken; // Safe to send to the server
        NSString *name = user.profile.name;
        NSString *email = user.profile.email;
        NSString *userPhotoURL = [[user.profile imageURLWithDimension:128] absoluteString];
        self.profileKey = [email stringByReplacingOccurrencesOfString:@"." withString:@""];
        [[self.ref childByAppendingPath:@"Users"] observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
            BOOL userNotNew = false;
            for (FDataSnapshot *child in snapshot.children) {
                if (![child.key isEqualToString:self.profileKey])  {
                    userNotNew = true;
                }
            }
            if (!userNotNew) {
                NSDictionary *newUserEmpty = @{
                                               @"name" : name,
                                               @"photo" : userPhotoURL,
                                               @"position" : @"",
                                               @"contacts" : email
                                               };
                [[[self.ref childByAppendingPath:@"Users"] childByAppendingPath:[email stringByReplacingOccurrencesOfString:@"." withString:@""]] setValue:newUserEmpty];
            }
        }];
        [self initUserDefaults];
        [self performSegueWithIdentifier:loginToMap sender:nil];
        
        NSLog(@"Customer details: %@ %@ %@ %@", userId, idToken, name, email);
        
        // ...
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
        [self toggleAuthUI];
    }
    @finally {
       // [self toggleAuthUI];
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:@"ToggleAuthUINotification"
     object:nil];
}

- (void)receiveToggleAuthUINotification:(NSNotification *) notification {
    if ([[notification name] isEqualToString:@"ToggleAuthUINotification"]) {
        [self toggleAuthUI];
     //   self.statusText.text = [notification userInfo][@"statusText"];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
