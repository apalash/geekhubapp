//
//  ParticipantsViewController.h
//  FBGHProject
//
//  Created by Anatoliy on 4/20/16.
//  Copyright © 2016 mobex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Firebase/Firebase.h>

@interface ParticipantsViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) NSString *courtName;
@property (strong, nonatomic) NSString *eventName;
@property (strong, nonatomic) NSString *userKey;
@property (nonatomic) BOOL onlyMyEvents;

- (void)showInView:(UIView *)aView animated:(BOOL)animated;

@end
