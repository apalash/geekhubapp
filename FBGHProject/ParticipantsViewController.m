//
//  ParticipantsViewController.m
//  FBGHProject
//
//  Created by Anatoliy on 4/20/16.
//  Copyright © 2016 mobex. All rights reserved.
//

#import "ParticipantsViewController.h"

@interface ParticipantsViewController ()
@property (strong, nonatomic) NSMutableArray *participants;
@property (strong, nonatomic) NSMutableArray *participantsPhoto;
@property (strong, nonatomic) NSMutableArray *participantsContact;
@property (strong, nonatomic) Firebase *participantsRef;
@property (weak, nonatomic) IBOutlet UICollectionView *collectioView;
@property (weak, nonatomic) IBOutlet UIButton *joinButton;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@end

@implementation ParticipantsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.6];
    self.collectioView.layer.cornerRadius = 5;
    self.collectioView.layer.shadowOpacity = 0.8;
    self.collectioView.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    [self.doneButton.layer setBorderColor:[[UIColor blueColor] CGColor]];
    self.collectioView.dataSource = self;
    self.collectioView.delegate = self;
    self.participantsRef = [[Firebase alloc] initWithUrl:@"https://fbghproject.firebaseio.com"];
    if (!self.onlyMyEvents) {
        [[[[[[self.participantsRef childByAppendingPath:@"Courts"] childByAppendingPath:self.courtName] childByAppendingPath:@"Events"] childByAppendingPath:self.eventName] childByAppendingPath:@"Participants"] observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
            self.participants = [[NSMutableArray alloc] init];
            self.participantsPhoto = [[NSMutableArray alloc] init];
            self.participantsContact = [[NSMutableArray alloc] init];
            for (FDataSnapshot *child in snapshot.children) {
                [self.participants addObject:child.key];
                if (([child.key isEqualToString:self.userKey]) &&
                    (![self.joinButton.titleLabel.text isEqualToString:@"Leave"])){
                    [self.joinButton setTitle:@"Leave" forState:UIControlStateNormal];
                    [self.joinButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                }
                [[[self.participantsRef childByAppendingPath:@"Users"] childByAppendingPath:child.key] observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
                    if ([snapshot.value[@"photo"] isEqualToString:@""]) {
                        [self.participantsPhoto addObject:[UIImage imageNamed:@"Image-6"]];
                    }
                    if ([snapshot.value[@"photoType"] isEqualToString:@"device"]) {
                        NSData *data = [[NSData alloc] initWithBase64EncodedString:snapshot.value[@"photo"] options:NSDataBase64DecodingIgnoreUnknownCharacters];
                        [self.participantsPhoto addObject:[UIImage imageWithData:data]];
                    } else if ([snapshot.value[@"photoType"] isEqualToString:@"url"]) {
                        NSURL *photoURL = [NSURL URLWithString:snapshot.value[@"photo"]];
                        NSData *data = [NSData dataWithContentsOfURL:photoURL];
                        [self.participantsPhoto addObject:[UIImage imageWithData:data]];
                        [self.collectioView reloadData];
                    }
                    [self.participantsContact addObject:snapshot.value[@"contacts"]];
                    [self.collectioView reloadData];
                }];
            }
        }];
    } else {
        [self.joinButton setTitle:@"Leave" forState:UIControlStateNormal];
        [self.joinButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [[self.participantsRef childByAppendingPath:@"Courts"] observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
            self.participants = [[NSMutableArray alloc] init];
            self.participantsPhoto = [[NSMutableArray alloc] init];
            self.participantsContact = [[NSMutableArray alloc] init];
            for (FDataSnapshot *court in snapshot.children) {
                if ([court.value[@"hasEvents"] isEqualToString:@"yes"]) {
                    [[[[self.participantsRef childByAppendingPath:@"Courts"] childByAppendingPath:court.key] childByAppendingPath:@"Events"] observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
                        self.participants = [[NSMutableArray alloc] init];
                        self.participantsPhoto = [[NSMutableArray alloc] init];
                        self.participantsContact = [[NSMutableArray alloc] init];
                        for (FDataSnapshot *eventInfo in snapshot.children) {
                            if ([eventInfo.key isEqualToString:self.eventName]) {
                                [[[[[[self.participantsRef childByAppendingPath:@"Courts"] childByAppendingPath:court.key] childByAppendingPath:@"Events"] childByAppendingPath:eventInfo.key] childByAppendingPath:@"Participants"] observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
                                    BOOL userParticipates = NO;
                                    self.participants = [[NSMutableArray alloc] init];
                                    self.participantsPhoto = [[NSMutableArray alloc] init];
                                    self.participantsContact = [[NSMutableArray alloc] init];
                                    for (FDataSnapshot *participant in snapshot.children) {
                                        [self.participants addObject:participant.key];
                                        [[[self.participantsRef childByAppendingPath:@"Users"] childByAppendingPath:participant.key] observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
                                            if ([snapshot.value[@"photo"] isEqualToString:@""]) {
                                                [self.participantsPhoto addObject:[UIImage imageNamed:@"Image-6"]];
                                            }
                                            if ([snapshot.value[@"photoType"] isEqualToString:@"device"]) {
                                                NSData *data = [[NSData alloc] initWithBase64EncodedString:snapshot.value[@"photo"] options:NSDataBase64DecodingIgnoreUnknownCharacters];
                                                [self.participantsPhoto addObject:[UIImage imageWithData:data]];
                                            } else if ([snapshot.value[@"photoType"] isEqualToString:@"url"]) {
                                                NSURL *photoURL = [NSURL URLWithString:snapshot.value[@"photo"]];
                                                NSData *data = [NSData dataWithContentsOfURL:photoURL];
                                                [self.participantsPhoto addObject:[UIImage imageWithData:data]];
                                                [self.collectioView reloadData];
                                            }
                                            [self.participantsContact addObject:snapshot.value[@"contacts"]];
                                            [self.collectioView reloadData];
                                        }];
                                        if ([participant.key isEqualToString:self.userKey]) {
                                            userParticipates = YES;
                                        }
                                    }
                                    if (!userParticipates) {
                                        self.participants = [[NSMutableArray alloc] init];
                                        self.participantsPhoto = [[NSMutableArray alloc] init];
                                        self.participantsContact = [[NSMutableArray alloc] init];
                                    } else {
                                        self.courtName = court.key;
                                       // [self.collectioView reloadData];
                                    }
                                }];
                            }
                        }
                    }];
                }
            }
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showInView:(UIView *)aView animated:(BOOL)animated {
    [aView addSubview:self.view];
    if (animated) {
        [self showAnimate];
    }
}

- (void)showAnimate {
    self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.view.alpha = 0;
    [UIView animateWithDuration:.25 animations:^{
        self.view.alpha = 1;
        self.view.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

- (void)removeAnimate {
    [UIView animateWithDuration:.25 animations:^{
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self.view removeFromSuperview];
        }
    }];
}

- (IBAction)doneButtonClicked:(id)sender {
    [self removeAnimate];
}

- (IBAction)joinButtonClicked:(id)sender {
    if ([self.joinButton.titleLabel.text isEqualToString:@"Leave"]) {
        [[[[[[[self.participantsRef childByAppendingPath:@"Courts"] childByAppendingPath:self.courtName] childByAppendingPath:@"Events"] childByAppendingPath:self.eventName] childByAppendingPath:@"Participants"] childByAppendingPath:self.userKey] removeValue];
        [self.joinButton setTitle:@"Join" forState:UIControlStateNormal];
        [self.joinButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        if (!self.onlyMyEvents) {
            [self.collectioView performBatchUpdates:^{
                [self.collectioView deleteItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:[self.participants indexOfObject:self.userKey] inSection:0]]];
                [self.participants removeObjectAtIndex:[self.participants indexOfObject:self.userKey]];
                if (![self.participantsPhoto count]) {
                    [self.participantsPhoto removeObjectAtIndex:[self.participants indexOfObject:self.userKey]];
                }
                if (![self.participantsContact count]) {
                    [self.participantsContact removeObjectAtIndex:[self.participants indexOfObject:self.userKey]];
                }
            } completion:nil];
        }
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:self.eventName];
        UILocalNotification *localNotification = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        [[UIApplication sharedApplication] cancelLocalNotification:localNotification];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:self.eventName];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else if ([self.joinButton.titleLabel.text isEqualToString:@"Join"]) {
        [[[[[[[self.participantsRef childByAppendingPath:@"Courts"] childByAppendingPath:self.courtName] childByAppendingPath:@"Events"] childByAppendingPath:self.eventName] childByAppendingPath:@"Participants"] childByAppendingPath:self.userKey] setValue:self.userKey];
        [self.joinButton setTitle:@"Leave" forState:UIControlStateNormal];
        [self.joinButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        //create new local notification
        [[[[[self.participantsRef childByAppendingPath:@"Courts"] childByAppendingPath:self.courtName] childByAppendingPath:@"Events"] childByAppendingPath:self.eventName] observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            formatter.dateFormat = @"dd-MM-yy HH:mm";
            NSDate *startDate = [formatter dateFromString:snapshot.value[@"startDate"]];
            UILocalNotification *localNotification = [[UILocalNotification alloc] init];
            localNotification.fireDate = [startDate dateByAddingTimeInterval:-1 * 60 * 60];
            localNotification.alertBody = [self.eventName stringByAppendingString:@" starts in an hour."];
            localNotification.timeZone = [NSTimeZone defaultTimeZone];
            localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
            localNotification.soundName = UILocalNotificationDefaultSoundName;
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:localNotification];
            [[NSUserDefaults standardUserDefaults] setObject:data forKey:self.eventName];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        }];
    }
}


#pragma mark - CollectionView DataSource
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    UIImageView *blurredImage = (UIImageView *)[cell viewWithTag:100];
    UIImageView *profileImage = (UIImageView *)[cell viewWithTag:200];
    UILabel *nameLabel = (UILabel *)[cell viewWithTag:300];
    nameLabel.text = [self.participantsContact objectAtIndex:indexPath.row];
    [blurredImage setImage:[self.participantsPhoto objectAtIndex:indexPath.row]];
    [profileImage setImage:[self.participantsPhoto objectAtIndex:indexPath.row]];
    return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (self.onlyMyEvents) {
        return [self.participantsPhoto count];
    } else {
        return [self.participants count];
    }
}

#pragma mark - CollectionView Layout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(200, 200);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 10, 90, 10);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
