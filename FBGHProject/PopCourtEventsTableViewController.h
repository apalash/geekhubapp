//
//  PopCourtEventsTableViewController.h
//  FBGHProject
//
//  Created by Anatoliy on 4/18/16.
//  Copyright © 2016 mobex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Firebase/Firebase.h>
#import "ParticipantsViewController.h"

@interface PopCourtEventsTableViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) NSString *courtKey;
@property (strong, nonatomic) NSString *userKey;
@end
