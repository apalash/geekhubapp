//
//  PopCourtEventsTableViewController.m
//  FBGHProject
//
//  Created by Anatoliy on 4/18/16.
//  Copyright © 2016 mobex. All rights reserved.
//

#import "PopCourtEventsTableViewController.h"

@interface PopCourtEventsTableViewController ()

@property (nonatomic, strong) NSMutableArray *courtEvents;

@end

@implementation PopCourtEventsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.courtEvents = [[NSMutableArray alloc] init];
    Firebase *courtEventsRef = [[Firebase alloc] initWithUrl:@"https://fbghproject.firebaseio.com/Courts"];
        [[courtEventsRef childByAppendingPath:self.courtKey] observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
            if (![self.tableView numberOfRowsInSection:0]) {
            if ([snapshot.value[@"hasEvents"] isEqualToString:@"yes"]) {
                [[[courtEventsRef childByAppendingPath:self.courtKey] childByAppendingPath:@"Events"] observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
                    if (![self.tableView numberOfRowsInSection:0]) {
                        for (FDataSnapshot *grandChild in snapshot.children) {
                            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                            formatter.dateFormat = @"dd-MM-yy";
                            NSString *currentDate = [formatter stringFromDate:[NSDate date]];
                            NSArray *dateComponents = [grandChild.value[@"startDate"] componentsSeparatedByString:@" "];
                            NSTimeInterval interval = [[formatter dateFromString:[dateComponents objectAtIndex:0]] timeIntervalSinceDate:[formatter dateFromString:currentDate]];
                            if (interval >= 0) {
                                [self.courtEvents addObject:grandChild.key];
                                if ([self.courtEvents count]) {
                                    [self.tableView reloadData];
                                }
                            } else {
                                NSUInteger eventCount = [snapshot childrenCount];
                                [[[[courtEventsRef childByAppendingPath:self.courtKey] childByAppendingPath:@"Events"] childByAppendingPath:grandChild.key] removeValueWithCompletionBlock:^(NSError *error, Firebase *ref) {
                                    if (eventCount == 1) {
                                        [[courtEventsRef childByAppendingPath:self.courtKey] observeEventType:FEventTypeChildRemoved withBlock:^(FDataSnapshot *snapshot) {
                                            if ([snapshot.value[@"Events"] isEqualToString:@""] ||
                                                !(snapshot.value[@"Events"])) {
                                                [[[courtEventsRef childByAppendingPath:self.courtKey] childByAppendingPath:@"hasEvents"] setValue:@"free"];
                                            }
                                        }];
                                    }
                                }];
                            }
                        }
                    }
                }];
            }
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.courtEvents count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }

    cell.textLabel.text = [self.courtEvents objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ParticipantsViewController *participantsViewController = [storyboard instantiateViewControllerWithIdentifier:@"Participants"];
    participantsViewController.courtName = self.courtKey;
    participantsViewController.eventName = selectedCell.textLabel.text;
    participantsViewController.userKey = self.userKey;
    participantsViewController.onlyMyEvents = NO;
  //  [self addChildViewController:participantsViewController];
    [participantsViewController showInView:self.view animated:YES];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
