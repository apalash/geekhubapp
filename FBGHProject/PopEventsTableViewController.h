//
//  PopEventsTableViewController.h
//  FBGHProject
//
//  Created by Anatoliy on 4/17/16.
//  Copyright © 2016 mobex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Firebase/Firebase.h>
#import "ParticipantsViewController.h"

@interface PopEventsTableViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSString *profileKey;

@end
