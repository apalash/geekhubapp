//
//  PopEventsTableViewController.m
//  FBGHProject
//
//  Created by Anatoliy on 4/17/16.
//  Copyright © 2016 mobex. All rights reserved.
//

#import "PopEventsTableViewController.h"

@interface PopEventsTableViewController ()
@property (nonatomic, strong) NSMutableArray *courtsNames;
@property (nonatomic, strong) NSMutableArray *events;
@property (nonatomic) BOOL isParticipant;
@end

@implementation PopEventsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.courtsNames = [[NSMutableArray alloc] init];
    self.events = [[NSMutableArray alloc] init];
    Firebase *firebaseRef = [[Firebase alloc] initWithUrl:@"https://fbghproject.firebaseio.com/Courts"];
    [firebaseRef observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        if (![self.tableView numberOfRowsInSection:0]) {
            for (FDataSnapshot *child in snapshot.children) {
                if ([child.value[@"hasEvents"] isEqualToString:@"yes"]) {
                    NSString *courtName = child.value[@"name"];
                    [self.courtsNames addObject:courtName];
                    Firebase *eventRef = [[firebaseRef childByAppendingPath:child.key] childByAppendingPath:@"Events"];
                    [eventRef observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
                        if (![self.tableView numberOfRowsInSection:0]) {
                            for (FDataSnapshot *child in snapshot.children) {
                                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                formatter.dateFormat = @"dd-MM-yy";
                                NSString *currentDate = [formatter stringFromDate:[NSDate date]];
                                NSArray *dateComponents = [child.value[@"startDate"] componentsSeparatedByString:@" "];
                                NSTimeInterval interval = [[formatter dateFromString:[dateComponents objectAtIndex:0]] timeIntervalSinceDate:[formatter dateFromString:currentDate]];
                                if (interval >= 0) {
                                    [[[eventRef childByAppendingPath:child.key] childByAppendingPath:@"Participants"] observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
                                        //    if (![self.tableView numberOfRowsInSection:0]) {
                                        for (FDataSnapshot *grandChild in snapshot.children) {
                                            if ([grandChild.key isEqualToString:self.profileKey]) {
                                                [self.events addObject:child.key];
                                                [self.tableView reloadData];
                                            }
                                        }
                                        //    }
                                    }];
                                }
                            }
                        }
                    }];
                }
            }
        }
    }];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (IBAction)doneClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.events count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    // Configure the cell...
    
    cell.textLabel.text = [self.events objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ParticipantsViewController *participantsViewController = [storyboard instantiateViewControllerWithIdentifier:@"Participants"];
   // participantsViewController.courtName = self.courtKey;
    participantsViewController.eventName = selectedCell.textLabel.text;
    participantsViewController.userKey = self.profileKey;
    participantsViewController.onlyMyEvents = YES;
    //  [self addChildViewController:participantsViewController];
    [participantsViewController showInView:self.view animated:YES];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
