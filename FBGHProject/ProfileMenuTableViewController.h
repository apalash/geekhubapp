//
//  ProfileMenuTableViewController.h
//  FBGHProject
//
//  Created by Anatoliy on 4/14/16.
//  Copyright © 2016 mobex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreImage/CoreImage.h>
#import <QuartzCore/QuartzCore.h>
#import "LaunchViewController.h"
#import "PopEventsTableViewController.h"

@interface ProfileMenuTableViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate, UIPopoverPresentationControllerDelegate, UIGestureRecognizerDelegate, UIImagePickerControllerDelegate, UIPopoverBackgroundViewMethods, UIPopoverControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *blurredImageView;
@property (nonatomic, strong) NSString *profileKey;

@end
