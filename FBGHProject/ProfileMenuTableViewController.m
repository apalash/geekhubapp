//
//  ProfileMenuTableViewController.m
//  FBGHProject
//
//  Created by Anatoliy on 4/14/16.
//  Copyright © 2016 mobex. All rights reserved.
//

#import "ProfileMenuTableViewController.h"
#import "Firebase.h"

@interface ProfileMenuTableViewController ()

@property (nonatomic, strong) Firebase *profileRef;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *contacts;
@property (weak, nonatomic) IBOutlet UILabel *gamePosition;
@property (weak, nonatomic) IBOutlet UIButton *eventsButton;

@end

@implementation ProfileMenuTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width / 2;
    self.profileImageView.clipsToBounds = YES;
    self.profileImageView.layer.borderWidth = 3.0f;
    self.profileImageView.layer.borderColor = [UIColor whiteColor].CGColor;
}

- (void)viewWillAppear:(BOOL)animated {
    self.profileRef = [[Firebase alloc] initWithUrl:@"https://fbghproject.firebaseio.com/Users"];
    [[self.profileRef childByAppendingPath:self.profileKey] observeEventType:FEventTypeValue  withBlock:^(FDataSnapshot *snapshot) {
        if ([snapshot.value[@"name"] isEqualToString:@""]) {
            self.nameLabel.text = self.profileKey;
        } else {
            self.nameLabel.text = snapshot.value[@"name"];
        }
        self.contacts.text = snapshot.value[@"contacts"];
        if ([snapshot.value[@"position"] isEqualToString:@""]) {
            self.gamePosition.text = @"Edit your game position";
            self.gamePosition.textColor = [UIColor lightGrayColor];
        } else {
            self.gamePosition.text = snapshot.value[@"position"];
            self.gamePosition.textColor = [UIColor blackColor];
        }
        if (![snapshot.value[@"photo"] isEqualToString:@""]) {
            if (![snapshot.value[@"photoType"] isEqualToString:@"device"]) {
                NSURL *photoURL = [NSURL URLWithString:snapshot.value[@"photo"]];
                NSData *data = [NSData dataWithContentsOfURL:photoURL];
                UIImage *img = [[UIImage alloc] initWithData:data];
                self.blurredImageView.image = img;
                self.profileImageView.image = img;
            } else if (![snapshot.value[@"photoType"] isEqualToString:@"url"]) {
                NSData *data = [[NSData alloc] initWithBase64EncodedString:snapshot.value[@"photo"] options:NSDataBase64DecodingIgnoreUnknownCharacters];
                self.blurredImageView.image = [UIImage imageWithData:data];
                self.profileImageView.image = [UIImage imageWithData:data];
                [self.tableView reloadData];
            }
        }        
    } withCancelBlock:^(NSError *error) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Not found" message:error.description preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }];
}

- (IBAction)profileEditButtonTapped:(id)sender {
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Edit profile" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *changePhoto = [UIAlertAction actionWithTitle:@"Change Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Choose source" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *librarySource = [UIAlertAction actionWithTitle:@"Photo from library" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:picker animated:YES completion:NULL];
        }];
        UIAlertAction *cameraSource = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            @try {
                UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                picker.delegate = self;
                picker.allowsEditing = YES;
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:picker animated:YES completion:NULL];
            }
            @catch (NSException *exception) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Camera is not avaliable on this device! Choose another source." preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self presentViewController:actionSheet animated:YES completion:nil];
                }];
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
            }
            @finally {
                
            }
        }];
        [actionSheet addAction:librarySource];
        [actionSheet addAction:cameraSource];
        [self presentViewController:actionSheet animated:YES completion:nil];
    }];
    [controller addAction:changePhoto];
    [controller addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Name";
    }];
    [controller addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Contacts";
    }];
    [controller addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Position";
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel
                                                   handler:nil];
    [controller addAction:cancel];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (![controller.textFields[0].text isEqualToString:@""]) {
            [[[self.profileRef childByAppendingPath:self.profileKey] childByAppendingPath:@"name"] setValue:controller.textFields[0].text];
            self.nameLabel.text = controller.textFields[0].text;
        }
        if (![controller.textFields[1].text isEqualToString:@""]) {
            [[[self.profileRef childByAppendingPath:self.profileKey] childByAppendingPath:@"contacts"] setValue:controller.textFields[1].text];
            self.contacts.text = controller.textFields[1].text;
        }
        if (![controller.textFields[2].text isEqualToString:@""]) {
            [[[self.profileRef childByAppendingPath:self.profileKey] childByAppendingPath:@"position"] setValue:controller.textFields[2].text];
            self.gamePosition.text = controller.textFields[2].text;
        }
    }];
    [controller addAction:ok];
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.blurredImageView.image = chosenImage;
    self.profileImageView.image = chosenImage;
    
    NSString *encodedPhoto = [UIImageJPEGRepresentation(chosenImage, 1.0f) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    [[[self.profileRef childByAppendingPath:self.profileKey] childByAppendingPath:@"photo"] setValue:encodedPhoto];
    [[[self.profileRef childByAppendingPath:self.profileKey] childByAppendingPath:@"photoType"] setValue:@"device"];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)eventsButtonTapped:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PopEventsTableViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"Pop"];
    controller.profileKey = self.profileKey;

    controller.modalPresentationStyle = UIModalPresentationPopover;
    [self presentViewController:controller animated:YES completion:nil];
    
    UIPopoverPresentationController *popController = [controller popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    popController.delegate = self;
    popController.sourceView = self.view;
    popController.sourceRect = self.eventsButton.frame;
}

# pragma mark - Popover Presentation Controller Delegate

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    
    // called when a Popover is dismissed
}

- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    
    // return YES if the Popover should be dismissed
    // return NO if the Popover should not be dismissed
    return YES;
}

- (void)popoverPresentationController:(UIPopoverPresentationController *)popoverPresentationController willRepositionPopoverToRect:(inout CGRect *)rect inView:(inout UIView *__autoreleasing  _Nonnull *)view {
    
    // called when the Popover changes position
}

- (IBAction)profileSignOutTapped:(id)sender {
    self.profileRef = [self.profileRef initWithUrl:@"https://fbghproject.firebaseio.com"];
    [self.profileRef unauth];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [self performSegueWithIdentifier:@"signOutSegue" sender:nil];
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"signOutSegue"]){
        UINavigationController *navController = (UINavigationController *)segue.destinationViewController;
        LaunchViewController *controller = (LaunchViewController *)navController.topViewController;
        controller.ref = self.profileRef;
        [controller.ref observeAuthEventWithBlock:^(FAuthData *authData) {
            
        }];
       // [controller dismissViewControllerAnimated:YES completion:nil];
    }
}

@end
