//
//  User.h
//  FBGHProject
//
//  Created by Anatoliy on 4/12/16.
//  Copyright © 2016 mobex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Firebase/Firebase.h"

@interface User : NSObject

@property (strong, nonatomic) NSString *uid;
@property (strong, nonatomic) NSString *email;

- (void)initUser:(FAuthData *)authData;

@end
