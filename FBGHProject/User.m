//
//  User.m
//  FBGHProject
//
//  Created by Anatoliy on 4/12/16.
//  Copyright © 2016 mobex. All rights reserved.
//

#import "User.h"

@implementation User

- (void)initUser:(FAuthData *)authData {
    self.uid = authData.uid;
    self.email = authData.providerData[@"email"];
}

@end
