//
//  MapViewController.h
//  
//
//  Created by Anatoliy on 4/5/16.
//
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "Court.h"
#import "CustomInfoWindow.h"
#import <CoreLocation/CoreLocation.h>
#import "ProfileMenuTableViewController.h"
#import "CourtDescriptionTableViewController.h"
#import <MMDrawerController.h>
#import <MMDrawerVisualState.h>
#import "AppDelegate.h"
#import "AddCourtViewController.h"

@import GoogleMaps;

@interface MapViewController : UIViewController <CLLocationManagerDelegate, GMSMapViewDelegate>

@property (strong, nonatomic) NSString *profileKey;

@end
