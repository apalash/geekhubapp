//
//  MapViewController.m
//  
//
//  Created by Anatoliy on 4/5/16.
//
//

#import "MapViewController.h"


static NSString * const kFirebaseURL = @"https://fbghproject.firebaseio.com";
static NSString * const mapToProfile = @"MapToProfile";

@interface MapViewController () 

@property (strong, nonatomic) User *user;
@property (nonatomic, strong) Firebase *ref;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) GMSMapView *mapView;
@property (strong, nonatomic) NSMutableArray *markers;
@property (strong, nonatomic) NSString *courtKey;
@end

@implementation MapViewController
- (void)viewDidLoad {
    [super viewDidLoad];
       
    self.ref = [[Firebase alloc] initWithUrl:kFirebaseURL];
    
    //location
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    //[self.locationManager requestAlwaysAuthorization];
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
    
    //Google maps
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:self.locationManager.location.coordinate.latitude
                                                            longitude:self.locationManager.location.coordinate.longitude
                                                                 zoom:12];
    self.mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    self.mapView.delegate = self;
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.icon = [GMSMarker markerImageWithColor:[UIColor purpleColor]];
    marker.position = camera.target;
    marker.snippet = @"Me";
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.map = self.mapView;

    self.view = self.mapView;
    
    [self.markers addObject:marker];
    
    Firebase *courtSearchRef = [[Firebase alloc] initWithUrl:@"https://fbghproject.firebaseio.com/Courts"];
    [courtSearchRef observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        for (FDataSnapshot *child in snapshot.children) {
            CLLocationCoordinate2D position = CLLocationCoordinate2DMake([child.value[@"latitude"] doubleValue],
                                                                         [child.value[@"longitude"] doubleValue]);
            GMSMarker *marker = [[GMSMarker alloc] init];//[GMSMarker markerWithPosition:position];
            if ([child.value[@"hasEvents"] isEqualToString:@"free"]) {
                marker.icon = [GMSMarker markerImageWithColor:[UIColor orangeColor]];
            } else {
                marker.icon = [GMSMarker markerImageWithColor:[UIColor redColor]];
            }
            marker.position = position;
            marker.title = child.value[@"name"];
            marker.userData = child.key;
            marker.appearAnimation = kGMSMarkerAnimationPop;
            marker.map = self.mapView;
            [self.markers addObject:marker];
        }
    }];
}


- (IBAction)showProfileButtonPressed:(id)sender {
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    
//    ProfileMenuTableViewController *leftDrawer = [storyboard instantiateViewControllerWithIdentifier:@"ProfileMenuTableViewController"];
//        leftDrawer.profileKey = self.profileKey;
//    
//    CATransition *transition = [CATransition animation];
//    transition.duration = 0.5;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFromLeft;
//    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
//    [self.view.layer addAnimation:transition forKey:nil];
//    [self.view addSubview:leftDrawer.view];
//    
//    [self.navigationController setNavigationBarHidden:YES animated:YES];
//    

     [self performSegueWithIdentifier:mapToProfile sender:nil];
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    ProfileMenuTableViewController *leftDrawer = [storyboard instantiateViewControllerWithIdentifier:@"ProfileMenuTableViewController"];
//    leftDrawer.profileKey = self.profileKey;
//    
//    MMDrawerController *drawerController = [[MMDrawerController alloc]
//                                             initWithCenterViewController:self
//                                             leftDrawerViewController:leftDrawer];
//    drawerController.openDrawerGestureModeMask = MMOpenDrawerGestureModePanningCenterView;
//    drawerController.closeDrawerGestureModeMask = MMCloseDrawerGestureModeTapCenterView;
//    
//    [drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished) {
//        
//    }];
//    [drawerController setDrawerVisualStateBlock:[MMDrawerVisualState slideAndScaleVisualStateBlock]];
//    
//    
//    ProfileMenuTableViewController *leftMenu = [[ProfileMenuTableViewController alloc] init];
//    [leftMenu.view setFrame:CGRectMake(200, 0, leftMenu.view.frame.size.width, leftMenu.view.frame.size.height)];
//    
//
//    
//    //your animation stuff...
//
//    [self addChildViewController:leftMenu];
//    [self.view addSubview:leftMenu.view];
//    [leftMenu didMoveToParentViewController:self];
    
}

- (IBAction)searchButtonPressed:(id)sender {
    [self.mapView clear];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:self.locationManager.location.coordinate.latitude
                                                            longitude:self.locationManager.location.coordinate.longitude
                                                                 zoom:12];
    self.mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    self.mapView.delegate = self;
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.icon = [GMSMarker markerImageWithColor:[UIColor purpleColor]];
    marker.position = camera.target;
    marker.snippet = @"Me";
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.map = self.mapView;
    
    self.view = self.mapView;
    
    [self.markers addObject:marker];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Search" message:@"Enter characteristics for a court" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Cover (rubbber/asphalt/wooden/concrete)";
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Event (yes/free)";
    }];
    
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self searchCourtWithCover:alertController.textFields[0].text
                             event:alertController.textFields[1].text];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self.mapView clear];
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:self.locationManager.location.coordinate.latitude
                                                                longitude:self.locationManager.location.coordinate.longitude
                                                                     zoom:12];
        self.mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
        self.mapView.delegate = self;
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.icon = [GMSMarker markerImageWithColor:[UIColor purpleColor]];
        marker.position = camera.target;
        marker.snippet = @"Me";
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.map = self.mapView;
        
        self.view = self.mapView;
        
        [self.markers addObject:marker];
        
        Firebase *courtSearchRef = [[Firebase alloc] initWithUrl:@"https://fbghproject.firebaseio.com/Courts"];
        [courtSearchRef observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
            for (FDataSnapshot *child in snapshot.children) {
                CLLocationCoordinate2D position = CLLocationCoordinate2DMake([child.value[@"latitude"] doubleValue],
                                                                             [child.value[@"longitude"] doubleValue]);
                GMSMarker *marker = [[GMSMarker alloc] init];//[GMSMarker markerWithPosition:position];
                if ([child.value[@"hasEvents"] isEqualToString:@"free"]) {
                    marker.icon = [GMSMarker markerImageWithColor:[UIColor orangeColor]];
                } else {
                    marker.icon = [GMSMarker markerImageWithColor:[UIColor redColor]];
                }
                marker.position = position;
                marker.title = child.value[@"name"];
                marker.userData = child.key;
                marker.appearAnimation = kGMSMarkerAnimationPop;
                marker.map = self.mapView;
                [self.markers addObject:marker];
            }
        }];

    }];
    [alertController addAction:cancel];
    [alertController addAction:ok];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)searchCourtWithCover:(NSString *)cover event:(NSString *)hasEvent {
    for (GMSMarker *marker in self.markers) {
        if (![marker.title isEqualToString:@"Me"]) {
            marker.map = nil;
        }
    }
    
 //   [self.mapView clear];
    
    
    Firebase *courtSearchRef = [[Firebase alloc] initWithUrl:@"https://fbghproject.firebaseio.com/Courts"];
    [courtSearchRef observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        BOOL foundCourt = false;
        for (FDataSnapshot *child in snapshot.children) {
            if (([cover isEqualToString:child.value[@"cover"]]) &&
                ([hasEvent isEqualToString:child.value[@"hasEvents"]])) {
                CLLocationCoordinate2D position = CLLocationCoordinate2DMake([child.value[@"latitude"] doubleValue],
                                                                             [child.value[@"longitude"] doubleValue]);
                GMSMarker *marker = [GMSMarker markerWithPosition:position];
                marker.icon = [GMSMarker markerImageWithColor:[UIColor greenColor]];
                marker.title = child.value[@"name"];
                marker.userData = child.key;
                marker.appearAnimation = kGMSMarkerAnimationPop;
                marker.map = self.mapView;
                foundCourt = true;
            }
        }
        if (!foundCourt) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Not found" message:@"There's no court with such characteristics" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    } withCancelBlock:^(NSError *error) {
        NSLog(@"%@", error.description);
    }];
}

- (void)addCourtWithName:(NSString *)courtName andCover:(NSString *)courtCover latitude:(float)latitude longitude:(float)longitude hasEvents:(NSString *)anyEvent {
    Firebase *courtAddRef = [[Firebase alloc] initWithUrl:@"https://fbghproject.firebaseio.com/Courts"];
    NSDictionary *newCourt = @{
                               @"name" : courtName,
                               @"cover" : courtCover,
                               @"latitude" : [NSNumber numberWithFloat:latitude],
                               @"longitude" : [NSNumber numberWithFloat:longitude],
                               @"hasEvents" : anyEvent,
                               @"photo" : @""
                               };
    [[courtAddRef childByAutoId] setValue:newCourt];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Location Manager

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    CLLocation *location = [locations lastObject];
//    CLLocationCoordinate2D coordinate = location.coordinate;
    
}

#pragma mark - Google Delegate Methods

- (void)mapView:(GMSMapView *)mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddCourtViewController *addCourtViewController = [storyboard instantiateViewControllerWithIdentifier:@"AddCourtViewController"];
    addCourtViewController.coordinate = coordinate;
    [self addChildViewController:addCourtViewController];
    [addCourtViewController showInView:self.view animated:YES];
}

//- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker {
//    CustomInfoWindow *infoWindow = [[[NSBundle mainBundle] loadNibNamed:@"InfoWindow" owner:self options:nil] objectAtIndex:0];
//    infoWindow.name.text = marker.title;
//    
//    return infoWindow;
//}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(nonnull GMSMarker *)marker {
    if (marker.userData != nil) {
        self.courtKey = marker.userData;
        [self performSegueWithIdentifier:@"showDetailsSegue" sender:nil];
    }
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"showDetailsSegue"]){
        CourtDescriptionTableViewController *controller = (CourtDescriptionTableViewController *)segue.destinationViewController;
        controller.courtKey = self.courtKey;
        controller.profileKey = self.profileKey;
    }
    if ([segue.identifier isEqualToString:@"MapToProfile"]) {
        ProfileMenuTableViewController *controller = (ProfileMenuTableViewController *)segue.destinationViewController;
        controller.profileKey = self.profileKey;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
